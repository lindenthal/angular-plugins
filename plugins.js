(function(angular) {

    /**
     * Logging helper. Checks if logging is available to prevent problems.
     */
    function log(msg) {
        if (window.console && window.console.log) window.console.log(msg);
    }
    
    if (window.registerPlugin) {
        log('Plugin framework already loaded.');
        return;
    }

    /**
     * Registers the plugin with another module.
     * Usage: window.registerPlugin('mypluginmodule', 'othermodule');
     */
    window.registerPlugin = function(pluginname, modulename) {
        var module;
        try {
            module = angular.module(modulename);
            module.requires.push(pluginname);
        } catch (e) {
            log('Module ' + modulename + ' not found.');
        }
    };
    
    /**
     * Defines registry module with the pluginRegistry service.
     * The service provider has the same methods, so you can use
     * the registry both in module.run() or module.config() factories.
     */
    angular.module('plugins', []).provider('pluginRegistry', function() {
        var hooks = {},
            self = this;

        /**
         * Adds a value, object or function for the specified hook name.
         * Usage: pluginRegistry.add('somehookname', somevalue);
         * Returns self for chaining.
         */
        this.add = function(hook, value) {
            if (!hooks.hasOwnProperty(hook)) {
                hooks[hook] = [];
            }
            hooks[hook].push(value);
            return self;
        };

        /**
         * Gets all values for the specified hook name in the
         * order they have been added.
         * Usage: pluginRegistry.all('somehookname');
         * Returns list of all added values or null.
         */
        this.all = function(hook) {
            return hooks[hook] || null;
        };

        /**
         * Gets first value for the specified hook name.
         * Usage: pluginRegistry.first('somehookname');
         * Returns single value or null.
         */
        this.first = function(hook) {
            var all = self.all(hook) || [null];
            return all[0];
        };

        /**
         * Checks if there is any value for the specified hook name.
         * Usage: if (pluginRegistry.has('somehookname')) { ... }
         * Returns true or false.
         */
        this.has = function(hook) {
            return !!self.first(hook);
        };

        /**
         * Provider factory method. Called by angular's $injector when an
         * instance of the pluginRegistry is required.
         * Returns itself.
         */
        this.$get = [function() { return self; }];
    });

}(window.angular));
